(function() {
    'use strict';
    var firebase = window.firebase || null;
    var app = {
        dbReference: null,
        init: function() {
            if (!!firebase) {
                this.initializeFirebase();
            }
            var btnQuery = document.getElementById('btnQuery');
            if (!!btnQuery) {
                btnQuery.addEventListener('click', this.submitQuery.bind(this));
            }
        },
        initializeFirebase: function() {
            var config = {
                apiKey: "AIzaSyA0hn3xYafN8z2Lp1CDHE0rvVdfCCf5qxE",
                authDomain: "ing-tech-challenge.firebaseapp.com",
                databaseURL: "https://ing-tech-challenge.firebaseio.com",
                storageBucket: "ing-tech-challenge.appspot.com",
                messagingSenderId: "26387209386"
            };
            firebase.initializeApp(config);
            this.dbReference = firebase.database().ref('Transactions');
        },
        submitQuery: function(e) {
            e.preventDefault();
            document.getElementById('results').textContent = 'success';
            // this.queryDatabase(null, null);
        },
        queryDatabase: function(date, customerId) {
            this.dbReference.orderByChild('CustomerId').equalTo(3).on('child_added', function(snapshot) {
                console.log(snapshot.val().Amount);
            });
        }
    };

    window.app = window.app || app;

    app.init();

}());