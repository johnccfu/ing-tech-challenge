'use strict';

/*
 * Unit tests for lib/app.js
 */

describe('App', function() {
    // API for interacting with the page
    var controls = {
        get results() {
            return document.getElementById('results').innerHTML;
        },
        get month() {
            return document.getElementById('dlMonth').value;
        },
        set month(val) {
            document.getElementById('dlMonth').value = val;
        },
        get year() {
            return document.getElementById('txtYear').value;
        },
        set year(val) {
            document.getElementById('txtYear').value = val;
        },
        get customerId() {
            return document.getElementById('txtCustomerId').value;
        },
        set customerId(val) {
            document.getElementById('txtCustomerId').value = val;
        },
        clickQuery: function() {
            document.getElementById('btnQuery').click();
        }
    };

    // inject the HTML fixture fo the tests
    beforeEach(function() {
        fixture.base = 'test/fixtures';
        fixture.load('queryform.fixture.html');

        app.init();
    });

    afterEach(function() {
        fixture.cleanup();
    });

    describe('initial', function() {
        it('should have months dropdown', function() {
            expect(document.querySelectorAll('#dlMonths').length).toBe(1);
        });
        it('should have 12 month items in the months dropdown', function() {
            expect(document.querySelectorAll('#dlMonths > option[value]').length).toBe(12);
        });
        it('should have an empty option as the first item in the months dropdown', function() {
            expect(document.querySelectorAll('#dlMonths > option')[0].getAttribute('value')).toBeNull();
        });
        it('should have year text input field', function() {
            expect(document.querySelectorAll('#txtYear').length).toBe(1);
        });
        it('should have customer id field', function() {
            expect(document.querySelectorAll('#txtCustomerId').length).toBe(1);
        });
        it('should have a query button', function() {
            expect(document.querySelectorAll('#btnQuery').length).toBe(1);
        });
    });

    describe('Clicking on query button', function() {
        it('should have called the submitQuery method once', function() {
            controls.clickQuery();
            expect(document.getElementById('results').textContent).toBe('success');
        });

    });
});